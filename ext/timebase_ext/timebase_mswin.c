/*
# Copyright (c) 2007 Jonas Pfenniger <zimbatm@oree.ch>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

#ifdef HAVE_WINDOWS_H

#include <timebase_ext.h>
#include <Winbase.h>

VALUE cWindowsClock;

static VALUE
cWindowClock_current_time(VALUE klass) {
    nsec_time_t time;

    if (!QueryPerformanceCounter(&time)) {
        // TODO : raise error
        //DWORD WINAPI GetLastError(void);
    }

    return ULL2NUM(time);
}

static VALUE
cWindowsClock_resolution(VALUE klass) {
    return INT2NUM(1);
}

static VALUE
cWindowsClock_frequency(VALUE klass) {
    nsec_time_t per_sec;

    // Returns the number of hits per second
    if (!QueryPerformanceFrequency(&per_sec)) {
        // TODO : raise error
        //DWORD WINAPI GetLastError(void);
    }

    return ULL2NUM(per_sec);
}

static VALUE
cWindowsClock_is_monotonic(VALUE klass) {
    return Qtrue; // NOTE : I am not sure about that one
}

void Init_timebase_ext() {
    mTimeBase = rb_define_class("TimeBase", rb_cObject);

    cWindowsClock = rb_define_class_under(cTimeBase, "WindowsClock", cTimeBase);
    rb_define_method(cWindowsClock, "current_time", cWindowsClock_current_time, 0);
    rb_define_method(cWindowsClock, "resolution",   cWindowsClock_resolution, 0);
    rb_define_method(cWindowsClock, "frequency",       cWindowsClock_frequency, 0);
    rb_define_method(cWindowsClock, "is_monotonic?",cWindowsClock_is_monotonic, 0);

}

#endif
