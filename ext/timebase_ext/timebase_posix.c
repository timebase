/*
# Copyright (c) 2007 Jonas Pfenniger <zimbatm@oree.ch>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
*/

#ifdef HAVE_UNISTD_H

#include <timebase_ext.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

VALUE cRealtimeClock;

static VALUE
cRealtimeClock_current_time(VALUE klass) {
    struct timespec tp;

    if (clock_gettime(CLOCK_REALTIME, &tp) < 0) {
        rb_sys_fail("RealtimeClock#current_time error");
    }

    nsec_time_t time = (nsec_time_t)tp.tv_sec * 10e9 + tp.tv_nsec;
    return ULL2NUM(time);
}

static VALUE
cRealtimeClock_resolution(VALUE klass) {
    struct timespec res;
    clock_getres(CLOCK_REALTIME, &res);

    nsec_time_t time = (nsec_time_t)res.tv_sec * 10e9 + res.tv_nsec;
    return ULL2NUM(time);
}

static VALUE
cRealtimeClock_frequency(VALUE klass) {
    return ULL2NUM(10e9);
}

static VALUE
cRealtimeClock_is_monotonic(VALUE klass) {
    return Qfalse; // FIXME : Not sure about that one
}

#ifdef _POSIX_MONOTONIC_CLOCK
VALUE cMonotonicClock;

static VALUE
cMonotonicClock_current_time(VALUE klass) {
    struct timespec tp;

    if (clock_gettime(CLOCK_MONOTONIC, &tp) < 0) {
        rb_sys_fail("MonotonicClock#current_time error");
    }

    nsec_time_t time = (nsec_time_t)tp.tv_sec * 10e9 + tp.tv_nsec;
    return ULL2NUM(time);
}

static VALUE
cMonotonicClock_resolution(VALUE klass) {
    struct timespec res;
    clock_getres(CLOCK_MONOTONIC, &res);

    nsec_time_t time = (nsec_time_t)res.tv_sec * 10e9 + res.tv_nsec;
    return ULL2NUM(time);
}

static VALUE
cMonotonicClock_frequency(VALUE klass) {
    return ULL2NUM(10e9);
}

static VALUE
cMonotonicClock_is_monotonic(VALUE klass) {
    return Qtrue;
}

#endif

void Init_timebase_ext() {
    cTimeBase = rb_define_class("TimeBase", rb_cObject);

    cRealtimeClock = rb_define_class_under(cTimeBase, "RealtimeClock", cTimeBase);
    rb_define_method(cRealtimeClock, "current_time", cRealtimeClock_current_time, 0);
    rb_define_method(cRealtimeClock, "resolution",   cRealtimeClock_resolution, 0);
    rb_define_method(cRealtimeClock, "frequency",       cRealtimeClock_frequency, 0);
    rb_define_method(cRealtimeClock, "is_monotonic?",cRealtimeClock_is_monotonic, 0);

#ifdef _POSIX_MONOTONIC_CLOCK

    cMonotonicClock = rb_define_class_under(cTimeBase, "MonotonicClock", cTimeBase);
    rb_define_method(cMonotonicClock, "current_time", cMonotonicClock_current_time, 0);
    rb_define_method(cMonotonicClock, "resolution",   cMonotonicClock_resolution, 0);
    rb_define_method(cMonotonicClock, "frequency",       cMonotonicClock_frequency, 0);
    rb_define_method(cMonotonicClock, "is_monotonic?",cMonotonicClock_is_monotonic, 0);

#endif

}

#endif
