require 'mkmf'

case RUBY_PLATFORM
when /cygwin|mingw|mswin32|bccwin32/i
  have_header("windows.h")
  if PLATFORM == /mswin/i
    # TODO : Use msvcrt or cygwin
    warn 'WARNING: mswin target not implemented yet'
  else # cross-compiling
    # headers should be found in /usr/i586-mingw32msvc/include/ on my debian machine
    warn 'WARNING: mswing cross-compiling not implemented yet'
    $:.unshift(File.expand_path(File.dirname(File.dirname(__FILE__))))
  end
  unless have_header("winbase.h")
    STDERR.puts 'ERROR: winbase.h not found'
    exit 1
  end
else # posix
  unless have_header("unistd.h")
    STDERR.puts 'ERROR: unistd.h not found'
    exit 1
  end
  unless have_library("rt", "clock_gettime")
    STDERR.puts "ERROR: librt not found"
    exit 1
  end
end

create_makefile "timebase_ext"

