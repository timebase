
$:.unshift File.expand_path(File.join(File.dirname(__FILE__), '..', 'lib'))

require 'test/unit'
require 'timebase'

class Test_TimeBase < Test::Unit::TestCase
  def test_available_clocks
    assert_nothing_raised { TimeBase.available_clocks }
    assert_kind_of Hash, TimeBase.available_clocks
  end

  def test_available_clocks_memoization
    old_id = TimeBase.available_clocks.object_id
    assert_equal old_id, TimeBase.available_clocks.object_id
    TimeBase.const_set(:TEST_MEMO_CONST, 4)
    assert_not_equal old_id, TimeBase.available_clocks.object_id
  end

  def test_get_clock
    assert_nil TimeBase.get_clock(:unknown)
    assert_kind_of TimeBase::SystemClock, TimeBase.get_clock(:unknown, true)
  end

end

class Test_TimeBaseClocks < Test::Unit::TestCase
  TimeBase.available_clocks.each_pair do |name, clock|
    define_method("test_#{name}_current_time") do
      c = clock.new

      smaller = true
      100.times do
        t1 = c.current_time
        t2 = c.current_time
        unless t1 < t2
          smaller = false
          break
        end
      end

      assert smaller, "Time should go in the future"
    end

    define_method("test_#{name}_no_segfaults") do
      c = clock.new

      c.is_monotonic?
      c.resolution
      c.frequency
    end
  end
end

