
class TimeBase

  # Platform:: JRuby
  class JavaClock < TimeBase
    # Implemented with java.lang.System.nanoTime()
    # 
    # See: TimeBase::Clock#current_time
    def current_time; end
  end
  # Platform:: POSIX compatible
  class RealtimeClock < TimeBase
    # See: TimeBase#current_time
    def current_time; end
    # See: TimeBase#resolution
    def resolution; end
    # Returns 10e9
    #
    # See: See: TimeBase#frequency
    def frequency; end
    # Returns false
    #
    # TimeBase#is_monotonic?
    def is_monotonic?; end
  end
  # Platform:: POSIX compatible
  class MonotonicClock < TimeBase
    # See: TimeBase#current_time
    def current_time; end
    # See: TimeBase#resolution
    def resolution; end
    # Returns 10e9
    #
    # See: See: TimeBase#frequency
    def frequency; end
    # Returns false
    #
    # TimeBase#is_monotonic?
    def is_monotonic?; end
  end
  # Platform:: MS-Windowns
  class WindowsClock < TimeBase
    # See: TimeBase#current_time
    def current_time; end
    # Returns 1
    #
    # See: TimeBase#resolution
    def resolution; end
    # Returns 10e9
    #
    # See: See: TimeBase#frequency
    def frequency; end
    # Returns true
    #
    # TimeBase#is_monotonic?
    def is_monotonic?; end
  end
end
