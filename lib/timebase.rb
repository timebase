#--
# Copyright (c) 2007 Jonas Pfenniger
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#++

# Abstract implementation of a clock, inherited by each specific clocks.
#
# Platform:: all
class TimeBase

  # Returns the clock's current time in nanoseconds. The time is not
  # correlated to the system's clock user representation.
  def current_time; raise NotImplementedError, "abstract method not overridden"; end

  # The resolution is a constant which indicated what the mimimum step of
  # current_time is.
  #
  # Returns nil if the information is not available.
  def resolution; nil; end

  # Returns how much times per second the clock is updated.
  #
  # Returns nil if the information is not available.
  def frequency; 10e9; end

  # Returns true if the clock is monotonic
  def is_monotonic?; false; end

  # Uses system clock ( gettimeofday )
  #
  # Platform:: all
  class SystemClock < TimeBase
    # See: TimeBase#current_time
    def current_time
      now = Time.now
      now.sec * 1_000_000_000 + now.usec * 1_000
    end

    # Return 1000
    #
    # See: TimeBase#resolution
    def resolution; 1000; end
  end
end

class << TimeBase

  # Returns a hash of available clocks.
  #
  # All clocks are taken from the TimeBase namespace
  def available_clocks
    c = constants
    if @_old_constants == c
      @_available_clocks
    else
      @_old_constants = c
      @_available_clocks = c.map do |cname|
        [cname, const_get(cname)]
      end.select do |obj|
        obj[1].kind_of?(Class) and 
        obj[1].ancestors.include?(TimeBase)
      end.inject(Hash.new) do |hash, obj|
        hash[obj[0].sub(/Clock$/, '').downcase] = obj[1]
        hash
      end
    end
  end

  # Returns a Clock instance if it is found by the clock_name designation.
  #
  # If fallback is set to true and no clock has been found, it returns
  # the system clock.
  def get_clock(name, fallback = false)
    name = name.to_s
    clock = self.available_clocks[name]
    unless clock
      if fallback
        clock = self.available_clocks['system']
      else
        clock = nil
      end
    end
    clock.new if clock
  end
  alias has_clock? get_clock
  alias [] get_clock

end

begin
  require 'timebase_ext'
rescue LoadError
  warn "WARNING: timebase extension not found"
end
