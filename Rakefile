require 'rake'
require 'rake/testtask'
require 'rake/rdoctask'
require 'rake/gempackagetask'

GEM_VERSION = "0.1"

desc "Remove build products"
task :clean do
  rm Dir.glob( "lib/**/*.jar" )
  rm Dir.glob( "lib/**/*.#{ Config::CONFIG['DLEXT'] }" )
  rm Dir.glob( "ext/**/Makefile" )
  rm Dir.glob( "ext/**/*.{o,#{ Config::CONFIG['DLEXT'] }}" )
  rm Dir.glob( "ext/**/mkmf.log" )
  rm Dir.glob( "java/**/*.{class,jar}" )
end

Rake::RDocTask.new do |task|
  task.rdoc_files.add [ 'README', 'LICENSE', 'lib/**/*.rb', 'stubs/**/*.rb' ]
end

task :clobber => [ :clean ]

def setup_extension( dir, lib_dir, extension )
  ext = File.join( "ext", dir )
  so_name = "#{ extension }.#{ Config::CONFIG['DLEXT'] }"
  ext_so = File.join( ext, so_name )
  lib_so = File.join( "lib", lib_dir, so_name )
  ext_files = FileList[ File.join( ext, "*.{c,h}" ) ]
  ext_makefile = File.join( ext, "Makefile" )
  extconf_rb = File.join( ext, "extconf.rb" )
  
  file ext_makefile => [ extconf_rb ] do
    Dir.chdir ext do
      ruby "extconf.rb"
    end
  end

  file ext_so => ext_files + [ ext_makefile ] do
    Dir.chdir ext do
      case PLATFORM
      when /win32/
        sh 'nmake'
      else
        sh 'make'
      end
    end
  end

  file lib_so => [ ext_so ] do
    cp ext_so, lib_so
  end

  task :compile => [ lib_so ]
end

def setup_jar( dir, lib_dir, extension )
  ext = File.join( "java", dir )
  jar_name = "#{ extension }.jar"
  ext_jar = File.join( ext, lib_dir, jar_name )
  lib_jar = File.join( "lib", lib_dir, jar_name )
  ext_files = FileList[ File.join( ext, "**", "*.java" ) ]
  main_source = File.join( lib_dir, "#{ extension.capitalize }Service.java" )

  file ext_jar => [ ext_files ] do
    Dir.chdir( ext ) do
      sh 'javac', '-classpath', File.join( ENV['JRUBY_HOME'], 'lib/jruby.jar' ), main_source
      sh 'jar', 'cf', File.join( lib_dir, jar_name ), *Dir.glob( '**/*.class' )
    end
  end

  file lib_jar => ext_jar do
    cp ext_jar, lib_jar
  end

  task :compile => [ lib_jar ]
end

case RUBY_PLATFORM
when /java/
  setup_jar( 'timebase_ext', '', 'timebase_ext' )
else
  setup_extension( 'timebase_ext', '', 'timebase_ext' )
end

desc "Compile extensions"
task :compile

task :test => [ :compile ]
Rake::TestTask.new do |task|
  task.ruby_opts << '-rrubygems'
  task.libs << 'lib'
  task.libs << 'test'
  task.test_files = [ "test/test_all.rb" ]
  task.verbose = true
end

desc "Loads the library into IRB"
task :console => [ :compile ] do
  sh "irb -Ilib -rirb/completion -rtimebase"
end

gemspec = Gem::Specification.new do |gemspec|
  gemspec.name = "timebase"
  gemspec.version = GEM_VERSION
  gemspec.author = "Jonas Pfenniger <zimbatm@oree.ch>"
  gemspec.summary = "Provides access to low-level system clocks"
  gemspec.test_file = 'test/test_all.rb'
  gemspec.files = FileList[ 'Rakefile', 'test/*.rb', 'ext/**/*.{c,h,rb}',
                            'stubs/**/*.rb', 'java/**/*.java',
                            "lib/**/*.{rb,jar,#{ Config::CONFIG['DLEXT'] }}" ]
  gemspec.require_paths = [ 'lib' ]
  gemspec.has_rdoc = true
  gemspec.extra_rdoc_files = FileList[ 'stubs/**/*.rb' ]

  case RUBY_PLATFORM
  when /java/
    gemspec.platform = 'jruby'
  when /win32/
    gemspec.platform = Gem::Platform::WIN32
  else
    gemspec.platform = Gem::Platform::RUBY
    gemspec.extensions = FileList[ 'ext/**/extconf.rb' ]
  end
end

task :package => [ :clean, :test ]
Rake::GemPackageTask.new( gemspec ) do |task|
  task.gem_spec = gemspec
  task.need_tar = true
end

task :default => [ :clean, :test ]

