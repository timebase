require "java"

class TimeBase
  # Platform:: java
  class JavaClock < TimeBase
    def initialize
      @sys = java.lang.System
    end

    # Implemented with java.lang.System.nanoTime()
    # 
    # See: TimeBase::Clock#current_time
    def current_time
      @sys.nanoTime()
    end
  end
end

