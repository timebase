// See : http://ola-bini.blogspot.com/2006/10/jruby-tutorial-4-writing-java.html

package timebase_ext;

import java.io.IOException;
import java.lang.System;

import org.jruby.*;

import org.jruby.Ruby;
import org.jruby.RubyClass;
import org.jruby.RubyException;
import org.jruby.RubyObject;
import org.jruby.exceptions.RaiseException;
import org.jruby.runtime.Block;
import org.jruby.runtime.CallbackFactory;
import org.jruby.runtime.callback.Callback;
import org.jruby.runtime.ObjectAllocator;
import org.jruby.runtime.load.BasicLibraryService;
import org.jruby.runtime.builtin.IRubyObject;

public class JavaClockService implements BasicLibraryService {
   public boolean basicLoad(final Ruby runtime) throws IOException {

       RubyModule mTimeBase = runtime.defineModule("TimeBase");
       RubyClass cClock = mTimeBase.defineClassUnder("Clock", runtime.getObject(), null);
       RubyClass cJavaClock = mTimeBase.defineClassUnder("JavaClock", cClock, null);

       CallbackFactory cf = runtime.callbackFactory(JavaClock.class);
       cJavaClock.defineMethod("current_time", cf.getMethod("current_time"));

       return true;
   }

   public static class JavaClock extends RubyObject {
       public JavaClock(Ruby runtime, RubyClass klass) {
           super(runtime, klass);
       }

       public long current_time() {
           return System.nanoTime();
       }
   }
}
